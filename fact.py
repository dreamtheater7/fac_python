# !/usr/bin/env python3
# -*-coding:cp1252-*-


def main():
    print("this is a factorial program.")

    # iteration:
    def fac_iteration(n, res):
        for i in range(1, n):
            res = res * i
        return res * n

    inp = 5
    result = fac_iteration(inp, 1)
    print(f"the factorial of {inp} is {result}")

    # recurision:
    def fac_recursion(n, data):
        if n < 1:
            return data
        else:
            data = data * n
            return fac_recursion(n - 1, data)

    inp_r = 5
    result_r = fac_recursion(inp_r, 1)
    print(f"the factorial of {inp_r} is {result_r}")

    
    

if __name__ == "__main__":
    main()
